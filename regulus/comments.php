<?php


// if comments are closed and is page then do nothing
if( !comments_open() && is_page() ) {

} else { ?>

<div id="comments">

	<h2><?php _e('Comments','regulus'); if ( comments_open() ) : ?><a href="#postComment" title="<?php _e('leave a comment','regulus'); ?>">&raquo;</a><?php endif; ?></h2>

	<?php

	global $usePassword;
	
	if ( $usePassword ) { ?>

		<p><?php _e('Enter your password to view comments','regulus'); ?></p>
		
 	<?php
 	
	} else if ( $comments ) {

		$commentCount = 1;

		?>

		<dl>

		<?php foreach ($comments as $comment) : 
		
			$class = bm_author_highlight();

		?>

			<dt class="<?php echo $class; ?>">
			<?php if( function_exists( "gravatar" ) ) { ?><img src="<?php gravatar("R", 30 ); ?>" /><?php } ?>
			<a href="#<?php comment_ID() ?>"><?php echo $commentCount."."; ?></a> <?php comment_author_link() ?> - <?php 
			comment_date();
			edit_comment_link(__('[Edit]','regulus'));?>
			</dt>
			<dd class="<?php echo $class; ?>">

			<?php

			comment_text();

			$commentCount ++; ?>
			</dd>

		<?php endforeach; ?>

		</dl>

	<?php } else { // If there are no comments yet

		if ( comments_open() ) {

			echo "<p>".__('no comments yet - be the first?','regulus')."</p>";
			
		} else {

			echo "<p>".__('Sorry comments are closed for this entry','regulus')."</p>";

		}
		
	} ?>
	
</div>

	<?php if ( comments_open() && !$usePassword ) { ?>

<form action="<?php echo get_settings('siteurl'); ?>/wp-comments-post.php" method="post" id="postComment">

	<input type="hidden" name="comment_post_ID" value="<?php echo $post->ID; ?>" />
	<input type="hidden" name="redirect_to" value="<?php echo wp_specialchars($_SERVER['REQUEST_URI']); ?>" />

	<label for="comment"><?php _e('message','regulus'); ?></label><br /><textarea name="comment" id="comment" tabindex="1"></textarea>

<?php

	if (isset($user_ID)) {
	
?>
		<p><?php _e('Logged in as', 'regulus');?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Logout &raquo;</a></p>
		
<?php

	} else {
	
?>
	
		<label for="author"><?php _e('name','regulus'); ?></label><input name="author" id="author" value="<?php echo $comment_author; ?>" tabindex="2" />
		<label for="email"><?php _e('email','regulus'); ?></label><input name="email" id="email" value="<?php echo $comment_author_email; ?>" tabindex="3" />
		<label for="url"><?php _e('url','regulus'); ?></label><input name="url" id="url" value="<?php echo $comment_author_url; ?>" tabindex="4" />
		
	<?php } ?>

	<input class="button" name="submit" id="submit" type="submit" tabindex="5" value="<?php _e('say it!','regulus'); ?>" />
	<?php do_action('comment_form', $post->ID); ?>
</form>

<?php

		}
	}
	
?>
