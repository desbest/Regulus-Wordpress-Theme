
		<ul id="footer">
			<li>&copy; 2005 <?php bloginfo('name'); ?></li>
			<!-- I'd appreciate the credit being left in, thanks in advance -->
			<li><a href="http://www.binarymoon.co.uk/" title="Regulus by Ben Gillbanks from Binary Moon - video games and emtertainment"><?php _e('Regulus by Ben @ Binary Moon','regulus'); ?></a></li>
			<li><a href="http://www.wordpress.org/" title="<?php _e('Powered by Wordpress','regulus'); ?>"><?php _e('Created with WordPress','regulus'); ?></a></li>
			<li><a href="#nav" title="<?php _e('Jump to top of page','regulus'); ?>"><?php _e('Top','regulus'); ?></a></li>
		</ul>
		
	</div>
	
	<?php do_action('wp_footer'); ?>
</body>
</html>
