<?php load_theme_textdomain('regulus'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
	<meta http-equiv="content-type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />

	<!-- feeds -->
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_get_archives('type=monthly&format=link'); ?>

	<link href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" rel="stylesheet" />
	<link href="<?php bloginfo('template_url'); ?>/switch.css" type="text/css" rel="stylesheet" />

	<?php wp_head();
	
	$headerImageURL = get_settings( 'regulus_headerImageURL' );
	
	if ( $headerImageURL != "" ) {

		echo "<style type=\"text/css\">
		
	#header {
	
		background:url( $headerImageURL ) #fff;
		
	}
		
</style>";

	}
	


	?>
	
	<!--
	Regulus Theme Created by Ben Gillbanks @ Binary Moon (http://www.binarymoon.co.uk/)
	-->
	
</head>

<?php

	// write the body tag.
	// needs some php fanciness to set the default header graphic

	// set default
	$headerImage = get_settings( 'regulus_headerImage' );
	$classExtra = "";
	
	if ( $headerImage == "" ) {
		$headerImage = "1";
	}
	
	if ( bm_getProperty( 'sidealign' ) == 1 ) {
		$classExtra = "leftAlign ";
	}
	
	$classExtra .= get_settings( 'regulus_colourScheme' );
	
	if ( $headerImageURL == "" ) {
		$classExtra .= " hid_$headerImage";
	}
	
	echo "<body class=\"$classExtra\">";

?>


<div id="wrapper">

	<div id="header">

		<?php
			$homeURL = get_settings( 'regulus_homeURL' );
			if( $homeURL == "" ) {
				$homeURL = get_settings('home');
			}
		?>
		<a href="<?php echo $homeURL; ?>" id="homeLink"><?php bloginfo('name'); ?></a>
		<?php if( bm_getProperty( 'heading' ) != 1 ) { ?>
		<h1><?php bloginfo('name'); ?></h1>
		<p class="site_description"><?php bloginfo('description'); ?></p>
		<?php } ?>

		<ul id="nav">
		<li <?php if (((is_home()) && !(is_paged())) or (is_archive()) or (is_single()) or (is_paged()) or (is_search())) { echo "class=\"current_page_item\""; } ?> >
			<a href="<?php echo get_settings('home'); ?>" ><?php if( get_settings( 'regulus_homeLink' ) == "" ) { _e('Home','regulus'); } else { echo get_settings( 'regulus_homeLink' ); } ?></a>
		</li>
		<?php wp_list_pages('sort_column=menu_order&depth=1&title_li='); ?>
		</ul>

	</div>
	
	<a href="#nav" class="skipnav"><?php _e('jump to navigation','regulus'); ?></a>
	


